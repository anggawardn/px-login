import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:px_login/ui/home.dart';
import 'package:px_login/ui/login.dart';
import 'package:px_login/ui/splash.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PX-login',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: MainScreen(),
    );
  }
}

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: FirebaseAuth.instance.onAuthStateChanged,
      builder: (context, AsyncSnapshot<FirebaseUser> snapshot){
        if (snapshot.connectionState == ConnectionState.waiting) {
          return SplashPage();
        }
        if (!snapshot.hasData || snapshot.data == null) {
          return LoginPage();
        }
        return HomePage();
      },
    );
  }
}
