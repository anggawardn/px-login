import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthProvider {
 final FirebaseAuth _auth = FirebaseAuth.instance;

 Future signInWithEmail(String email, String password) async {
   try {
    AuthResult result = await _auth.signInWithEmailAndPassword(email: email, password: password);
    FirebaseUser user = result.user;
    Future getToken() async {
        var tokenz = await user.getIdToken(refresh: false);
        print('idToken: ' + tokenz.token);
      }
    getToken();

    if (user != null) {
      return true;
    } else {
      return false; 
    }
   } catch (e) {
     return false;
   }
 }

 Future<bool> signInWithGoogle() async{
   try {
     GoogleSignIn googleSignIn = GoogleSignIn();
     GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
     if (googleSignInAccount == null) {
       return false;
     }
     AuthResult res = await _auth.signInWithCredential(GoogleAuthProvider.getCredential(
       idToken: (await googleSignInAccount.authentication).idToken, 
       accessToken: (await googleSignInAccount.authentication).accessToken
      ));
      if (res.user == null) {
        return false;
      }

      FirebaseUser user =  res.user;
      Future getToken() async {
        var tokenz = await user.getIdToken(refresh: false);
        print('idToken: ' + tokenz.token);
      }
      getToken();

      return true;
   } catch (e) {
     print("Error Logging with google");
     return false;
   }
 }
 
 Future<void> logOut() async {
   try {
     await _auth.signOut();
   } catch (e) {
     print("error logging out");
   }
 }
}