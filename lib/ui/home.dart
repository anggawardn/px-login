import 'package:flutter/material.dart';
import 'package:px_login/utils/firebase_auth.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Page')
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Home Page'),
            RaisedButton(
              child: Text("Log Out"),
              onPressed: () {
                AuthProvider().logOut();
              }
            )
          ],
        ),
      ),
    );
  }
}