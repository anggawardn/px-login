import 'package:flutter/material.dart';
import 'package:px_login/utils/firebase_auth.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _emailController;
  TextEditingController _passwordController;
  
  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController(text: '');
    _passwordController = TextEditingController(text: '');
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const SizedBox(height: 10.0,),
              Text('Login', style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20.0
                ),
              ),
              const SizedBox(height: 20.0,),
              RaisedButton(
                child: Text("Login with Google"),
                onPressed: () async {
                  bool res = await AuthProvider().signInWithGoogle();
                  if (!res){
                    print("Error Logging with google");
                  }
                }),
              const SizedBox(height: 10.0),
              TextField(
                controller: _emailController,
                obscureText: false,
                decoration: InputDecoration(
                  hintText: 'Enter email'
                ),
              ),
              const SizedBox(height: 10.0),
              TextField(
                controller: _passwordController,
                obscureText: true,
                decoration: InputDecoration(
                  hintText: 'Enter password'
                ),
              ),
              const SizedBox(height: 10.0,),
              RaisedButton(
                child: Text('Login'),
                onPressed: () async {
                  if (_emailController.text.isNotEmpty && _passwordController.text.isNotEmpty) {
                  bool res = await AuthProvider().signInWithEmail(_emailController.text, _passwordController.text);
                  if (!res) {
                    print("Login Failed with mail");
                  }
                  } else {
                    print("Email and Password cannot be empty");
                  }
                },
              )
            ],
          )
        ),
      ),
    );
  }
}